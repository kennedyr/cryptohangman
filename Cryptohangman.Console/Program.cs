﻿using CryptoHangman.Library;
using CryptoHangman.StructureMap;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Cryptohangman.Console
{
    class Program
    {
        private static Regex inputRegex = new Regex(@"([A-z])[= ]([A-z])");
        private static int levelOne = 10;
        static void Main(string[] args)
        {
            ObjectFactory.Initialize(x =>
            {
                x.AddRegistry<CommonRegistry>();
            });
            System.Console.WriteLine("Solve the Cryptogram to reveal the hidden message.");
            printGameUsage();
            var gameService = ObjectFactory.GetInstance<IGameService>();
            var game = gameService.NewGame(levelOne);
            printGameStatus(game);
            while (!game.GameOver)
            {
                var input = System.Console.ReadLine();
                var match = inputRegex.Match(input);
                if (match.Success && match.Groups.Count == 3)
                {
                    var cipherStr = match.Groups[1].Value;
                    var replacementStr = match.Groups[2].Value;
                    if (cipherStr.Length == 1 && replacementStr.Length == 1)
                    {
                        game = gameService.Guess(game, cipherStr.First(), replacementStr.First());
                        printGameStatus(game);
                    }
                }
                else
                {
                    printGameUsage();
                }
            }
            if (game.Won)
            {
                System.Console.WriteLine("Congratulations!  You Won!");
            }
            else
            {
                System.Console.WriteLine("Game Over.");
                System.Console.WriteLine("The message was: {0}", game.ClearText);
            }
        }
        private static void printGameStatus(IGame game)
        {
            System.Console.WriteLine(String.Format("{0} Guesses left", game.MaxIncorrect - game.Incorrect));
            if (game.Incorrect > 0) 
            {
                System.Console.WriteLine();
                System.Console.WriteLine(String.Join("", Enumerable.Range(0, game.Incorrect).Select(i => "X")));
            } 
            System.Console.WriteLine(game.CipherText);
            System.Console.WriteLine(game.PlayerText);
        }
        private static void printGameUsage()
        {
            System.Console.WriteLine("Enter the cipher character followed by the real character, for example: 's=p' or 's p'");
        }
    }
}
