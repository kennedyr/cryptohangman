﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CryptoHangman.Web.Startup))]
namespace CryptoHangman.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
