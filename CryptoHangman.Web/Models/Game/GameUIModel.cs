﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CryptoHangman.Web.Models.Game
{
    public class GameUIModel : IGameUIModel
    {
        public string CipherText { get; set; }
        public string PlayerText { get; set; }
        public HashSet<KeyValuePair<char, char>> PlayerGuesses { get; set; }
        public int Turns { get; set; }
        public int Incorrect { get; set; }
        public bool GameOver { get; set; }
        public int MaxIncorrect { get; set; }
        public bool Won { get; set; }
    }

    public interface IGameUIModel
    {
        string CipherText { get; }
        string PlayerText { get; }
        HashSet<KeyValuePair<char, char>> PlayerGuesses { get; }
        int Turns { get; }
        int Incorrect { get; }
        bool GameOver { get; }
        int MaxIncorrect { get; }
        bool Won { get; }
    }
}