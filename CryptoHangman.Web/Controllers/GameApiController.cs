﻿using CryptoHangman.Library;
using CryptoHangman.Web.Models.Game;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CryptoHangman.Web.Controllers
{
    public class GameApiController : ApiController
    {
        public IGameService GameService { get; set; }

        public GameApiController(IGameService gameService)
        {
            GameService = gameService;
        }

        public IGameUIModel Guess(int gameID, char cryptChar, char clearChar)
        {
            IGame game = LoadGame(gameID);
            if (game != null)
            {
                if (GameService.Validate(game, cryptChar, clearChar))
                {
                    game = GameService.Guess(game, cryptChar, clearChar);
                    SaveGame(gameID, game);
                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        ReasonPhrase = "Request Validation Failed",
                        Content = new StringContent("The request is not valid for the current game state.")
                    });
                }
                return (IGameUIModel) new GameUIModel().InjectFrom(game);
            }
            return null;
        }

        private void SaveGame(int gameID, IGame game)
        {
            throw new NotImplementedException();
        }

        private IGame LoadGame(int gameID)
        {
            throw new NotImplementedException();
        }
    }
}
