﻿using CryptoHangman.Library.Phrase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;

namespace CryptoHangman.Domain.Phrase
{
    public class PhraseService : IPhraseService
    {
        public string GetPhrase(int p)
        {
            if(p > 0)
                try
                {
                    string all;
                    using (var reader = new StreamReader(Assembly.GetExecutingAssembly()
                        .GetManifestResourceStream("CryptoHangman.Domain.Quotes.QuotesFile.txt")))
                    {
                        all = reader.ReadToEnd();
                    }
                    var allLines = all.Split('\n');
                    return allLines[p % allLines.Length].Trim();
                }
                catch
                {
                
                }
            return null;
        }
    }
}
