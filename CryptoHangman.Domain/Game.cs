﻿using CryptoHangman.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CryptoHangman.Domain
{
    internal class Game : IGame
    {
        public string ClearText { get; set; }
        public string CipherText { get; set; }
        public string PlayerText { get; set; }

        public HashSet<Tuple<char, char, bool>> PlayerGuesses { get; set; }
        public int Turns { get; set; }
        public int Incorrect { get; set; }
        public bool GameOver { get; set; }
        public int MaxIncorrect { get; set; }
        public bool Won 
        { 
            get 
            { 
                return PlayerText == ClearText; 
            } 
        }
    }
}
