﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CryptoHangman.Library;
using CryptoHangman.Library.Cipher;
using CryptoHangman.Library.Phrase;
using System.Text.RegularExpressions;
using Omu.ValueInjecter;
using System.Diagnostics;

namespace CryptoHangman.Domain
{
    public class GameService : IGameService
    {
        public ICipher Cipher { get; set; }
        public IPhraseService PhraseService { get; set; }
        public Func<int> Random { get; set; }

        protected Char Placeholder = '_';
        protected Regex CharRegex = new Regex("[A-z]");

        public GameService(ICipher cipher, IPhraseService phraseService, Func<int> random)
        {
            Cipher = cipher;
            PhraseService = phraseService;
            Random = random;
        }

        public IGame NewGame(int maxTries)
        {
            var clearText = PhraseService.GetPhrase(Random());
            return new Game
            {
                ClearText = clearText,
                CipherText = Cipher.Encrypt(clearText),
                PlayerText = CharRegex.Replace(clearText, Placeholder.ToString()),
                PlayerGuesses = new HashSet<Tuple<char, char, bool>>(),
                MaxIncorrect = maxTries,
            };
        }

        public IGame Guess(IGame game, char cryptChar, char clearChar)
        {
            var newGameState = (Game)new Game().InjectFrom(game);
            if (Validate(game, cryptChar, clearChar))
            {
                bool isCorrect = IsCorrect(game.CipherText, game.ClearText, cryptChar, clearChar);
                game.PlayerGuesses.Add(new Tuple<char, char, bool>(Char.ToUpper(cryptChar), Char.ToUpper(clearChar), isCorrect));
                if (isCorrect)
                {
                    var indexes = getIndexesOf(game.CipherText, cryptChar);
                    var text = newGameState.PlayerText.ToCharArray();
                    foreach (var index in indexes)
                    {
                        text[index] = Char.IsUpper(game.CipherText[index]) ? Char.ToUpper(clearChar) : Char.ToLower(clearChar);
                    }
                    newGameState.PlayerText = new String(text);
                }
                else
                {
                    newGameState.Incorrect++;
                }
                if (newGameState.Won || newGameState.Incorrect > newGameState.MaxIncorrect)
                    newGameState.GameOver = true;
                newGameState.Turns++;
            }
            return newGameState;
        }

        public bool Validate(IGame game, char cryptChar, char clearChar)
        {
            bool dupe = game.PlayerGuesses.Any(g => g.Item1 == Char.ToUpper(cryptChar) && g.Item2 == Char.ToUpper(clearChar));
            bool exists = game.CipherText.IndexOf(cryptChar.ToString(), StringComparison.OrdinalIgnoreCase) >= 0;
            bool alreadySolved = game.PlayerGuesses.Any(g => g.Item1 == Char.ToUpper(cryptChar) && g.Item3);
            return !game.GameOver && !dupe && exists && !alreadySolved;
        }

        private bool IsCorrect(string cipherText, string clearText, char cryptchar, char clearChar){
            var index = cipherText.IndexOf(cryptchar.ToString(), StringComparison.OrdinalIgnoreCase);
            if (index >= 0 && index < clearText.Length)
                return Char.ToUpper(clearChar) == Char.ToUpper(clearText[index]);
            return false;
        }

        private IEnumerable<int> getIndexesOf(string haystack, char needle)
        {
            for (int i = 0; i < haystack.Length; i++)
            {
                if(Char.ToUpper(haystack[i]) == Char.ToUpper(needle))
                    yield return i;
            }
        }
    }
}
