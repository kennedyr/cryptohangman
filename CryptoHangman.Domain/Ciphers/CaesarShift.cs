﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CryptoHangman.Library.Cipher;

namespace CryptoHangman.Domain.Ciphers
{
    public class CaesarShift : ICipher
    {
        protected string Alphabet { get; set; }
        protected Func<int> Rand { get; set; }

        public CaesarShift(Func<int> random, string alphabet)
        {
            Alphabet = alphabet;
            Rand = random;
        }

        public string Encrypt(string clearText)
        {
            var dict = new Dictionary<char, char>();
            int offset = Rand();
            for (int i = 0; i < Alphabet.Length; i++)
            {
                dict.Add(Alphabet[i], Alphabet[getIndex(i + offset, Alphabet.Length)]);
            }
            return new String(clearText.Select(c => getValue(dict, c)).ToArray());
        }

        private char getValue(Dictionary<char,char> lookup, char c)
        {
            if (Char.IsLetter(c))
            {
                var compareValue = Char.ToUpper(c);
                if (lookup.ContainsKey(compareValue))
                {
                    var newValue = lookup[compareValue];
                    return Char.IsUpper(c) ? newValue : Char.ToLower(newValue);
                }
                else
                    throw new ArgumentException("Unknown Letter Character.");
            }
            return c;
        }
        //unnecessary
        private int getIndex(int i, int mod)
        {
            var result = i % mod;
            if (result < 0)
                return mod + result;
            return result;
        }
    }
}
