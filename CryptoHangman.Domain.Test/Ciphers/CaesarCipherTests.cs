﻿using CryptoHangman.Domain.Ciphers;
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CryptoHangman.Domain.Test.Ciphers
{
    [TestClass]
    public class CaesarCipherTests
    {
        public const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        [TestMethod]
        public void NaiveShift()
        {
            var x = new CaesarShift(() => 0, alphabet);
            var clearText = "Hello world!";
            var cipherText = x.Encrypt(clearText);
            Assert.AreEqual(clearText, cipherText);
        }

        [TestMethod]
        public void SimpleShift()
        {
            var x = new CaesarShift(() => 1, alphabet);
            var clearText = "Hello world!";
            var cipherText = x.Encrypt(clearText);
            Assert.AreEqual("Ifmmp xpsme!", cipherText);
        }

        [TestMethod]
        public void WrapAroundShift()
        {
            var x = new CaesarShift(() => 28, alphabet);
            var clearText = "Hello world!";
            var cipherText = x.Encrypt(clearText);
            Assert.AreEqual("Jgnnq yqtnf!", cipherText);
        }

        [TestMethod]
        public void ReverseShift()
        {
            var x = new CaesarShift(() => -1, alphabet);
            var clearText = "Hello world!";
            var cipherText = x.Encrypt(clearText);
            Assert.AreEqual("Gdkkn vnqkc!", cipherText);
        }

        [TestMethod]
        public void ReverseWrapAroundShift()
        {
            var x = new CaesarShift(() => -28, alphabet);
            var clearText = "Hello world!";
            var cipherText = x.Encrypt(clearText);
            Assert.AreEqual("Fcjjm umpjb!", cipherText);
        }
    }
}
