﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CryptoHangman.Library.Cipher
{
    public interface ICipher
    {
        string Encrypt(string clearText);
    }
}
