﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CryptoHangman.Library.Phrase
{
    public interface IPhraseService
    {
        string GetPhrase(int p);
    }
}
