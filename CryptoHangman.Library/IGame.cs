﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoHangman.Library
{
    public interface IGame
    {
        string ClearText { get; }
        string CipherText { get; }
        string PlayerText { get; }
        HashSet<Tuple<char, char, bool>> PlayerGuesses { get; }
        int Turns { get; }
        int Incorrect { get; }
        bool GameOver { get; }
        int MaxIncorrect { get; }
        bool Won { get; }
    }
}
