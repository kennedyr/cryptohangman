﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CryptoHangman.Library
{
    public interface IGameService
    {
        IGame NewGame(int maxTries);
        IGame Guess(IGame game, char cryptChar, char clearChar);
        bool Validate(IGame game, char cryptChar, char clearChar);
    }
}
