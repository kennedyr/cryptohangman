#CryptoHangman
A simple word game based off of hangman.  Decode the cryptogram to reveal the hidden message.

##Roadmap

###Functional Style
* Refactor the core library to be functional.
* Port it to F# as a learning exercise.

###UI
* Build a web site frontend.
* Build mobile app frontends for Android, IOS, and WinPhone.
* Build a twitter service so that you can play via tweets (This idea was shamelessly stolen from John Skeet).
* Build a Win8 Desktop frontend.

###Game Play
* Add different skill levels.
  * Add hints and a "seeded" cryptogram for beginners.
  * Add more difficult ciphers for more advanced levels.
* Add scoring and high scores.
* Adaptively rank message difficulty based on player's success or, if I'm feeling ambitious, some sort of language analysis.
* Add feature to use real messages from a player's social media.