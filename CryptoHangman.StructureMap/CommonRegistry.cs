﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StructureMap;
using StructureMap.Configuration.DSL;
using StructureMap.Graph;
using CryptoHangman.Library.Phrase;
using CryptoHangman.Domain.Phrase;
using CryptoHangman.Library;
using CryptoHangman.Domain;
using CryptoHangman.Library.Cipher;
using CryptoHangman.Domain.Ciphers;

namespace CryptoHangman.StructureMap
{
    public class CommonRegistry : Registry
    {
        protected const string Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public CommonRegistry()
        {
            For<IPhraseService>().Use<PhraseService>();
            var gameRand = new Random();
            For<IGameService>().Use<GameService>().Ctor<Func<int>>("random").Is(() => { return gameRand.Next(); });
            var cipherRand = new Random();
            For<ICipher>().Use<CaesarShift>().Ctor<Func<int>>("random").Is(() => { return cipherRand.Next(); }).Ctor<string>("alphabet").Is(Alphabet);
        }
    }
}
